package boot

import (
	"fmt"
	"gp_kanxun_vr/library/redis"
	_ "gp_kanxun_vr/packed"

	"github.com/gogf/gf/frame/g"
)

func init() {

	//redis初始化
	err := redis.Init()
	if err != nil {
		g.Log().Error("redis初始化失败", err)
		fmt.Printf("redis初始化失败，%v\n", err)
		return
	}

}
