package router

import (
	"gp_kanxun_vr/app/api"
	"gp_kanxun_vr/library/jwt"

	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
)

func init() {
	s := g.Server()
	s.Group("/", func(group *ghttp.RouterGroup) {
		//允许跨域访问
		group.Middleware(MiddlewareCORS)
		//登录接口
		group.POST("/login", jwt.GfJWTMiddleware.LoginHandler)
		//JWT认证中间件
		group.Middleware(MiddlewareAuth)
		//测试
		group.GET("/hello", api.Hello.Index)
		//注销
		group.ALL("/logout", jwt.GfJWTMiddleware.LogoutHandler)
	})
}

// MiddlewareCORS 跨域
func MiddlewareCORS(r *ghttp.Request) {
	r.Response.CORSDefault()
	r.Middleware.Next()
}

// MiddlewareAuth authHook is the HOOK function implements JWT logistics.
func MiddlewareAuth(r *ghttp.Request) {
	jwt.GfJWTMiddleware.MiddlewareFunc()(r)
	r.Middleware.Next()
}
