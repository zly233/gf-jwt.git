module gp_kanxun_vr

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-redis/redis/v8 v8.11.0
	github.com/gogf/gf v1.16.4
	github.com/gogf/gf-jwt v1.1.3
)

go 1.14
