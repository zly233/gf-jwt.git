package service

import (
	"gp_kanxun_vr/app/dao"
	"gp_kanxun_vr/library/response"

	"github.com/gogf/gf/frame/g"
)

// Login 用户登录逻辑
func Login(username string) (userID int64, err error) {
	usUser, err := dao.UsUser.QueryUserName(username)
	if err != nil {
		g.Log().Error("dao.UsUser.Count()查询异常", err)
		return
	}
	//查询不到数据，用户不存在
	if len(usUser) == 0 {
		return 0, response.ErrorUserNotExist
	}
	//查询数据异常
	if len(usUser) > 1 {
		return 0, response.ErrorDataException
	}
	//查询数据成功
	if len(usUser) == 1 {
		userID = usUser[0].Id
		return
	}

	return 0, response.ErrorDataException
}
