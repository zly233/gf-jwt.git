package api

import (
	"errors"
	"gp_kanxun_vr/app/dao"
	"gp_kanxun_vr/app/service"
	"gp_kanxun_vr/library/response"
	"net/http"
	"time"

	"github.com/gogf/gf/util/gconv"

	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
)

var User = userApi{}

type userApi struct{}

// Login 用户登录
func (*userApi) Login(r *ghttp.Request) (interface{}, error) {
	userName := r.GetString("username")
	id, err := service.Login(userName)
	if err != nil {
		//g.Log().Error("异常错误")
		if errors.Is(err, response.ErrorUserNotExist) {
			//response.JsonExit(r, 1, "no", "用户不存在")
			return nil, response.ErrorUserNotExist
		}
		if errors.Is(err, response.ErrorDataException) {
			//response.JsonExit(r, 1, "no", "数据异常")
			return nil, response.ErrorDataException
		}
	}
	//到redis中查询id，是否当前用户登陆过
	userIS, err := dao.UsUser.QueryUserIDRedis(id)
	if userIS {
		//返回重复登录，请重新登录
		return nil, response.ErrorUserRepetition
	}
	//传递id值到r中
	r.SetParam("id", id)
	return g.Map{
		"id":       id,
		"username": userName,
	}, nil
}

// LoginResponse 登录成功后的响应函数，在此存储数据到redis
func (*userApi) LoginResponse(r *ghttp.Request, code int, token string, expire time.Time) {
	//调用redis存储数据
	err := dao.UsUser.SetUserNameRedis(token, gconv.Int64(r.GetParam("id")))
	if err != nil {
		g.Log().Error(" redis.Rdb.Set()异常", err)
		return
	}
	err = r.Response.WriteJson(g.Map{
		"code":   http.StatusOK,
		"token":  token,
		"expire": expire.Format(time.RFC3339),
	})
	if err != nil {
		g.Log().Error("r.Response.WriteJson()解析失败", err)
		return
	}
	r.ExitAll()
}

// LogoutResponse 注销成功,删除redis中的数据
func (*userApi) LogoutResponse(r *ghttp.Request, code int) {
	err := dao.UsUser.DeleteUserIDRedis(gconv.Int64(r.GetParam("id")))
	if err != nil {
		g.Log().Error(" dao.UsUser.DeleteUserIDRedis()异常", err)
		return
	}
	err = r.Response.WriteJson(g.Map{
		"code":    code,
		"message": "success",
	})
	if err != nil {
		g.Log().Error("r.Response.WriteJson()解析失败", err)
		return
	}
	r.ExitAll()
}
