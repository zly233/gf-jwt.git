package main

import (
	_ "gp_kanxun_vr/boot"
	_ "gp_kanxun_vr/router"

	"github.com/gogf/gf/frame/g"
)

func main() {
	g.Server().Run()
}
