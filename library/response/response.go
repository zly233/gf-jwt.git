package response

import (
	"errors"

	"github.com/gogf/gf/net/ghttp"
)

// JsonResponse 数据返回通用JSON数据结构
type JsonResponse struct {
	Code    int         `json:"code"`    // 错误码((0:成功, 1:失败, >1:错误码))
	Message string      `json:"message"` // 提示信息
	Data    interface{} `json:"data"`    // 返回数据(业务接口定义具体数据结构)
}

// ErrorUserNotExist 定义异常错误处理
var (
	ErrorUserNotExist   = errors.New("用户不存在")
	ErrorDataException  = errors.New("数据异常")
	ErrorUserRepetition = errors.New("重复登录,请重新登录")
)

// Json 标准返回结果数据结构封装。
func Json(r *ghttp.Request, code int, message string, data ...interface{}) {
	responseData := interface{}(nil)
	if len(data) > 0 {
		responseData = data[0]
	}
	err := r.Response.WriteJson(JsonResponse{
		Code:    code,
		Message: message,
		Data:    responseData,
	})
	if err != nil {
		r.Response.Writeln("JSON数据结构封装错误")
		return
	}
}

// JsonExit 返回JSON数据并退出当前HTTP执行函数。
func JsonExit(r *ghttp.Request, err int, msg string, data ...interface{}) {
	Json(r, err, msg, data...)
	r.Exit()
}
