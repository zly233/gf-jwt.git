package jwt

import (
	"gp_kanxun_vr/app/api"
	"time"

	auth "github.com/gogf/gf-jwt/example/api"

	jwt "github.com/gogf/gf-jwt"
	"github.com/gogf/gf/frame/g"
)

var GfJWTMiddleware *jwt.GfJWTMiddleware

func init() {
	middleware, err := jwt.New(&jwt.GfJWTMiddleware{
		Realm:         "KanXun_vr",                                        //领域名称
		Key:           g.Cfg().GetBytes("JWT.MySecret"),                   //签名秘钥
		Timeout:       time.Minute * 60,                                   //过期时间
		MaxRefresh:    time.Minute * 5,                                    //token过期后，可凭借旧token获取新token的刷新时间
		IdentityKey:   "id",                                               // 身份验证的key值
		TokenLookup:   "header: Authorization, query: token, cookie: jwt", // token检索模式，用于提取token-> Authorization
		TokenHeadName: "Bearer",                                           // token在请求头时的名称，默认值为Bearer
		// 客户端在header中传入Authorization 对一个值是Bearer + 空格 + token
		//TimeFunc:      time.Now,       // 测试或服务器在其他时区可设置该属性
		Authenticator:   api.User.Login,          // 根据登录信息对用户进行身份验证的回调函数
		Unauthorized:    auth.Unauthorized,       // 处理不进行授权的逻辑
		IdentityHandler: auth.IdentityHandler,    // 解析并设置用户身份信息
		PayloadFunc:     auth.PayloadFunc,        // 登录期间的回调的函数
		LoginResponse:   api.User.LoginResponse,  // 登录成功后的响应，在此处添加数据到redis
		LogoutResponse:  api.User.LogoutResponse, //注销/token无效化（黑名单）处理后返回的信息，用户可自定义返回数据
	})
	if err != nil {
		g.Log().Error(err)
		return
	}
	GfJWTMiddleware = middleware
}
