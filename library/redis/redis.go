package redis

import (
	"context"
	"fmt"
	"time"

	"github.com/gogf/gf/frame/g"

	"github.com/go-redis/redis/v8" // 注意导入的是新版本
)

var (
	Rdb *redis.Client
)

func Init() (err error) {
	Rdb = redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%v:%d", g.Cfg().GetString("redis.host"), g.Cfg().GetInt("redis.port")),
		Password: "",                                // no password set
		DB:       g.Cfg().GetInt("redis.db"),        // use default DB
		PoolSize: g.Cfg().GetInt("redis.pool_size"), // 连接池大小
	})

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	_, err = Rdb.Ping(ctx).Result()
	return err
}

func Close() {
	_ = Rdb.Close()
}
